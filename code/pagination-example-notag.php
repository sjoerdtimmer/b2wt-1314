<?php
$this->load->library('pagination');

$pag['base_url']='http://example.com/controller/functie/';
$pag['total_rows'] = 200;
$pag['per_page'] = 20; 

$this->pagination->initialize($pag); 

echo $this->pagination->create_links();
?>